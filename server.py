import sys
import json
import helmut

from twisted.internet import reactor
from twisted.python import log
from twisted.web.server import Site
from twisted.web.static import File

from autobahn.twisted.websocket import WebSocketServerFactory, WebSocketServerProtocol
from autobahn.twisted.resource import WebSocketResource

MSG_TYPE_TURN = "turn"
MSG_TYPE_TACK = "tack"

MSG_ATTR_DEG = "degrees"
MSG_ATTR_DIRECTION = "direction"

class CholitaServerProtocol(WebSocketServerProtocol):
    def onConnect(self, request):
        print("WebSocket connection request: {}".format(request))

    def onMessage(self, payload, isBinary):
        msg = json.loads(payload)

        if msg['type'] == MSG_TYPE_TURN:
            degrees = msg[MSG_ATTR_DEG]
            print("Turing {} degrees".format(degrees))
            helmut.changeCourse(degrees)

        if msg['type'] == MSG_TYPE_TACK:
            direction = msg[MSG_ATTR_DIRECTION]
            print("Tacking {}".format(direction))
            helmut.tack(direction)

        self.sendMessage(payload, isBinary)

if __name__ == '__main__':
    
    helmut.setup_GPIO()
    
    log.startLogging(sys.stdout)

    # Serve `dist` as root to access index.html
    root = File("dist")

    # Serve `dist` as /dist to access javascript resources
    assets = File("dist")
    root.putChild("dist", assets)

    # Connect WebSockets on /ws
    factory = WebSocketServerFactory(u"ws://127.0.0.1:5000")
    factory.protocol = CholitaServerProtocol
    root.putChild(u"ws", WebSocketResource(factory))

    # Create a Site object, listen on 5000
    reactor.listenTCP(5000, Site(root))
    reactor.run()
    
    helmut.cleanup_GPIO()
