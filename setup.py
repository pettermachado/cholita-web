from setuptools import setup, find_packages

reqs = [line.rstrip() for line in open('requirements.txt')]
packages = find_packages()

setup(name='cholita-web',
    version='0.0',
    description="",
    url='',
    author='',
    author_email='',
    packages=packages,
    setup_requires=reqs,
    install_requires=reqs,
    scripts=[
        'server.py',
        ]
    )