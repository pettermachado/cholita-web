# cholita-web

Control center for Cholita

## Getting started

### Node

Install [Node.js](https://nodejs.org/en/) with npm

```shell
$ npm install # install node and npm if you have not
$ npm run build # to prepare html, javascript and css files
```

### Python

Install [Miniconda](https://conda.io/docs/#) and setup an environment for cholita

```shell
conda create -n cholita python=2.7
source activate cholita
pip install -f requirements.txt
```

Now everything should be installed, run the application with

```shell
$ python app.py
```

and visit [http://localhost:5000](http://localhost:5000) in your browser

## Developing

Run `npm run watch` to have javascript and css assets recompiled on changes.

# Pi setup

To enable the Raspberry Pi to act as a Wifi access point, work with GPIO and run the webserver follow these steps.

*TODO: copy Pi setup steps here*