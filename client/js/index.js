import React from 'react'
import ReactDOM from 'react-dom'
import CholitaWSClient from './ws'

import Connectivity from './components/connectivity'
import Controls from './components/controls'
import History from './components/history'

// CSS requires
import material from 'materialize-css/dist/css/materialize.min.css'
import materialIcons from '../css/icons.css'

// Images
import hs120 from '../assets/touch-icon-120x120.png'
import hs152 from '../assets/touch-icon-152x152.png'
import hs167 from '../assets/touch-icon-167x167.png'
import hs180 from '../assets/touch-icon-180x180.png'

const host = window.location.host
const scheme = window.location.protocol.indexOf('https') === 0 ? 'wss' : 'ws'
let wsURL = scheme + '://' + host + '/ws'

if (ENV !== 'production') {
  wsURL = 'ws://localhost:5000/ws'
  console.log('Development backend', wsURL)
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      connected: false,
      events: []
    }

    const handlers = [
      'onMessage',
      'didDisconnect',
      'didConnect',
      'handleTurn',
      'handleTack'
    ]
    handlers.forEach(h => {
      this[h] = this[h].bind(this)
    })
  }

  componentDidMount() {
    setTimeout(() => {
      this.client = new CholitaWSClient(
        wsURL,
        this.onMessage,
        this.didConnect,
        this.didDisconnect
      )
    }, 1500)
  }

  onMessage(msg, err) {
    if (msg.type) {
      msg.at = new Date()
      console.log('Message', msg)
      const events = this.state.events
        .concat([msg])
      this.setState({ events })
    } else {
      console.log('Unexpected non-JSON message', msg, err)
    }
  }

  didConnect() {
    this.setState({ connected: true })
  }

  didDisconnect() {
    this.setState({ connected: false })
  }

  handleTurn(degrees) {
    this.client.turn(degrees)
  }

  handleTack(starboard) {
    this.client.tack(starboard)
  }

  render() {
    const common = { connected: this.state.connected }
    return <div>
      <Connectivity {...common} />
      <History events={this.state.events} />
      <div className="container">
        <Controls
          {...common}
          onTurn={this.handleTurn}
          onTack={this.handleTack}
        />
      </div>
    </div>
  }
}

ReactDOM.render(<App />, document.getElementById('app'))
