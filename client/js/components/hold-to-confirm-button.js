import React from 'react'
import PropTypes from 'prop-types'

class HoldToConfirmButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = { confirming: false, count: 0, timeout: 0 }
    const handlers = [
      'handleDown',
      'handleUp',
      'handleCountdown'
    ]
    handlers.forEach(h => {
      this[h] = this[h].bind(this)
    })
  }

  handleDown() {
    clearTimeout(this.state.timeout)
    this.setState({confirming: true, count: this.props.milliseconds}, this.handleCountdown)
  }
  handleUp() {
    clearTimeout(this.state.timeout)
    this.setState({confirming: false, count: this.props.milliseconds})
  }
  handleCountdown() {
    if (!this.state.confirming) { return }
    if (this.state.count <= 0) {
      this.handleUp()
      return this.props.onConfirm()
    }
    const timeout = setTimeout(this.handleCountdown, 10)
    this.setState({count: this.state.count - 10, timeout})
  }

  render() {
    const cx = `hold-to-confirm ${this.props.className}`
    const btnCx = `btn ${this.props.btnClassName}`
    const progressCx = `progress ${this.props.progressClassName}`
    const progress = this.state.confirming ? 100-(this.state.count/this.props.milliseconds)*100 : 0
    return <div className={cx}>
      <div className={progressCx} style={{width: `${progress}%`}}></div>
      <button
        className={btnCx}
        onMouseDown={this.handleDown}
        onTouchStart={this.handleDown}
        onMouseUp={this.handleUp}
        onTouchEnd={this.handleUp}
      >
        {this.props.children}
      </button>
    </div>
  }
}
HoldToConfirmButton.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  milliseconds: PropTypes.number.isRequired,
  btnClassName: PropTypes.string,
  progressClassName: PropTypes.string,
  progressColor: PropTypes.string
}
HoldToConfirmButton.defaultProps = {
  milliseconds: 5000
}

export default HoldToConfirmButton
