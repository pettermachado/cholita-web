import React from 'react'
import PropTypes from 'prop-types'

const translation = {
  online: {
    label: 'Uppkopplad',
    icon: 'sync',
    className: 'blue'
  },
  offline: {
    label: 'Nedkopplad',
    icon: 'sync_disabled',
    className: 'blue-grey'
  }
}
class Connectivity extends React.Component {
  render() {
    const connected = this.props.connected ? 'online' : 'offline'
    const cx = 'connectivity connectivity--' + connected
    return <div className="navbar-fixed">
      <nav className={translation[connected].className}>
        <div className="nav-wrapper container">
          <span className="brand-logo left">
            Cholita
          </span>
          <ul className="right">
            <li>
              <i className="material-icons">{translation[connected].icon}</i>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  }
}
Connectivity.propTypes = {
  connected: PropTypes.bool
}
export default Connectivity
