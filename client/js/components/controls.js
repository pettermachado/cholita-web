import React from 'react'
import PropTypes from 'prop-types'
import HoldToConfirmButton from './hold-to-confirm-button'
import Button from './button'

function Half(props) {
  const className = props.className ? props.className : ''
  return <div {...props} className={`col s6 ${className}`}></div>
}

class Controls extends React.Component {
  handleTurn(degrees) {
    this.props.onTurn(degrees)
  }

  handleTack(starboard) {
    this.props.onTack(starboard)
  }

  button(label, icon, iconPosition, color, onClick) {
    const disabled = this.props.connected ? '' : 'disabled'
    return <Button className={`btn-large wide ${color} ${disabled}`} onClick={onClick}>
      {(!!icon && iconPosition === 'left') && <i className="material-icons left">{icon}</i>}
      {label}
      {(!!icon && iconPosition === 'right') && <i className="material-icons right">{icon}</i>}
    </Button>
  }
  turnButton(amount) {
    const neg = amount < 0
    const label = Math.abs(amount)
    const icon = neg ? 'remove' : 'add'
    const iconPosition = neg ? 'left' : 'right'
    const color = neg ? 'pink' : 'teal'
    const onClick = this.handleTurn.bind(this, amount)
    return this.button(label, icon, iconPosition, color, onClick)
  }
  tackButton(starboard) {
    const disabled = this.props.connected ? '' : 'disabled'
    const label = starboard ? 'Styrbord' : 'Babord'
    const color = starboard ? 'teal' : 'pink'
    const onConfirm = this.handleTack.bind(this, starboard)
    return <HoldToConfirmButton
      milliseconds={1500}
      onConfirm={onConfirm}
      btnClassName={`btn-large wide ${color} ${disabled}`}
      progressClassName={`${color} lighten-3`}
    >
      {label}
    </HoldToConfirmButton>
  }

  render() {
    return <div className="controls">
      <h5>Kurs</h5>
      <div className="row">
        <Half>{this.turnButton(-10)}</Half>
        <Half>{this.turnButton(10)}</Half>
      </div>
      <div className="row">
        <Half>{this.turnButton(-1)}</Half>
        <Half>{this.turnButton(1)}</Half>
      </div>
      <h5>Slag</h5>
      <div className="row">
        <Half>{this.tackButton(false)}</Half>
        <Half>{this.tackButton(true)}</Half>
      </div>
    </div>
  }
}
Controls.propTypes = {
  onTurn: PropTypes.func.isRequired,
  onTack: PropTypes.func.isRequired
}
export default Controls
