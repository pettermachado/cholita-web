import React from 'react'

function Button(props) {
  const className = props.className ? props.className : ''
  return <button {...props} className={`btn ${className}`} />
}

export default Button
