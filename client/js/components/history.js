import React from 'react'

const i18n = {
  turn: {
    type: 'Kurs',
    suff: '°',
    val(e) {
      return e.degrees
    }
  },
  tack: {
    type: 'Slag',
    suff: '',
    val(e) {
      return e.direction === 'starboard'
        ? 'Styrbord'
        : 'Babord'
    }
  }
}
function padJoin(items, sep = ':') {
  return items
    .map(i => i < 10 ? '0'+i : i)
    .join(sep)
}
function DisplayEvent(props) {
  const e = props.event
  if (!e) {
    return <div className="chip">Ingen historik</div>
  }
  const t = i18n[e.type]
  const d = padJoin([
    e.at.getHours(),
    e.at.getMinutes(),
    e.at.getSeconds(),
    e.at.getMilliseconds()
  ])
  return <div className="chip">
    {t.type} {t.val(e)}{t.suff} @ {d}
  </div>
}

class History extends React.Component {
  constructor(props) {
    super(props)
    this.state = {offset: 0}

    const handlers = [
      'handlePrev',
      'handleNext'
    ]
    handlers.forEach(h => {
      this[h] = this[h].bind(this)
    })
  }

  handlePrev() {
    this.setState({offset: this.state.offset+1})
  }
  handleNext() {
    this.setState({offset: this.state.offset-1})
  }

  render() {
    const last = this.props.events.length - 1
    const offset = this.state.offset
    const hasNext = offset > 0
    const hasPrev = offset < last
    const index = last - this.state.offset

    return <div className="history">
      <div className="container">
        <DisplayEvent event={this.props.events[index]}/>
        <div className="right">
          <a className={`btn btn-flat ${hasPrev ? '' : 'disabled'}`} onClick={this.handlePrev}><i className="material-icons">chevron_left</i></a>
          <a className={`btn btn-flat ${hasNext ? '' : 'disabled'}`} onClick={this.handleNext}><i className="material-icons">chevron_right</i></a>
        </div>
      </div>
    </div>
  }
}

export default History
