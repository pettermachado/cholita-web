import {w3cwebsocket as W3CWebSocket} from 'websocket'
import uuid from './lib/uuid'

function CholitaWSClient(
  url,
  messageCallback,
  connectCallback,
  disconnectCallback
) {
  this.messageCallback = messageCallback
  this.connectCallback = connectCallback
  this.disconnectCallback = disconnectCallback

  this.client = new W3CWebSocket(url);
  this.client.onerror = this.onerror.bind(this)
  this.client.onopen = this.onopen.bind(this)
  this.client.onclose = this.onclose.bind(this)
  this.client.onmessage = this.onmessage.bind(this)
}

CholitaWSClient.prototype.onerror = function(err) {
  console.log('error', err)
}

CholitaWSClient.prototype.onopen = function(/*event*/) {
  if (this.connectCallback) {
    this.connectCallback()
  }
}

CholitaWSClient.prototype.onclose = function(/*event*/) {
  if (this.disconnectCallback) {
    this.disconnectCallback()
  }
}

CholitaWSClient.prototype.onmessage = function(msg) {
  if (this.messageCallback) {
    const {data} = msg
    try {
      const json = JSON.parse(data)
      this.messageCallback(json)
    } catch (err) {
      this.messageCallback(data, err)
    }
  }
}

CholitaWSClient.prototype.turn = function(degrees) {
  console.log('turning', degrees, 'deg')
  this.send({ type: 'turn', degrees })
}

CholitaWSClient.prototype.tack = function(starboard) {
  const direction = starboard ? 'starboard' : 'port'
  console.log('tacking', direction)
  this.send({ type: 'tack', direction })
}

CholitaWSClient.prototype.send = function(msg) {
  msg.id = uuid()
  console.log('Sending', msg)
  const str = JSON.stringify(msg)
  this.client.send(str)
}

export default CholitaWSClient
