PLUS_TEN_PIN = 17#2
PLUS_ONE_PIN = 4#3
MINUS_TEN_PIN = 2#17
MINUS_ONE_PIN = 3#4
SIGNAL_DELAY_SLEEP_TIME = 0.1
SIGNAL_MAPPING_DICT = {PLUS_ONE_PIN:'PLUS_ONE',PLUS_TEN_PIN:'PLUS_TEN',MINUS_ONE_PIN:'MINUS_ONE',MINUS_TEN_PIN:'MINUS_TEN'}

def setup_GPIO():
    """ function for setting up the GPIO pins - Run before seding signals"""
    import RPi.GPIO as GPIO
    import sys
    GPIO.setmode(GPIO.BCM)
    initiateGPIOpins()

def cleanup_GPIO():
    """ GPIO cleanup function - Run after sending signals """
    import RPi.GPIO as GPIO
    GPIO.cleanup()

def initiateGPIOpins():
    """ Initiate the GPIO pins """
    import RPi.GPIO as GPIO
    for pin in [PLUS_TEN_PIN, PLUS_ONE_PIN, MINUS_TEN_PIN, MINUS_ONE_PIN]:
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.HIGH)

def sendSignal(pinList):
    """ Function for sending signal on GPIO pins.
    Takes a python list as input, the list contains the pins to send signal on.
    The pins could be specified using integers or using the predefined flasgs:
        PLUS_TEN_PIN
        PLUS_ONE_PIN
        MINUS_TEN_PIN
        MINUS_ONE_PIN
    """
    import time
    import RPi.GPIO as GPIO
    import sys    
    for pin in pinList:
        sys.stderr.write('LOGMSG: Sending signal on pin '+SIGNAL_MAPPING_DICT[pin]+'\n')
        GPIO.output(pin, GPIO.LOW)
    time.sleep(SIGNAL_DELAY_SLEEP_TIME)
    for pin in pinList:
        GPIO.output(pin, GPIO.HIGH)
    time.sleep(SIGNAL_DELAY_SLEEP_TIME)

def tack(direction):
    """ Send signal on two pins to tack, takes a string with value + or - that specifies the direction of the tack"""
    if   direction == "starboard": sendSignal([PLUS_TEN_PIN,PLUS_ONE_PIN])
    elif direction == "port": sendSignal([MINUS_TEN_PIN,MINUS_ONE_PIN])
    else: print "Error: tack not valid!" 

def changeCourse(degrees):
    """Function for changing course.
    Sends signal on appropriate pins.
    """
    import sys
    if degrees == 'tack+' or degrees == "starboard":
        tack("starboard")
        return
    elif degrees == 'tack-' or degrees == "port":
        tack("port")
        return
    else: degrees = int(degrees)
    if degrees > 0:
        for ten_degree_step in range(degrees/10): sendSignal([PLUS_TEN_PIN])
        remainingDegrees = degrees%10
        if remainingDegrees <= 5:
            for ten_degree_step in range(remainingDegrees): sendSignal([PLUS_ONE_PIN])
        else:
            sendSignal([PLUS_TEN_PIN])
            for ten_degree_step in range(10-remainingDegrees): sendSignal([MINUS_ONE_PIN])
    elif degrees < 0:
        for ten_degree_step in range(-degrees/10): sendSignal([MINUS_TEN_PIN])
        remainingDegrees = -degrees%10
        if remainingDegrees <= 5:
            for ten_degree_step in range(remainingDegrees): sendSignal([MINUS_ONE_PIN])
        else:
            sendSignal([MINUS_TEN_PIN])
            for ten_degree_step in range(10-remainingDegrees): sendSignal([PLUS_ONE_PIN])
    else:
        sys.stderr.write('WARNING: no change detected course change is zero degrees!\n')