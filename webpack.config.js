const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = (env = {}) => {
  const isProd = !!env.production
  const e = isProd ? 'production' : 'development'

  console.log(`  ==> Webpack ENV: ${e}`)
  console.log(`  ==> Extracting CSS: ${isProd}\n`)

  const config = {
    entry: [
      path.join(__dirname, 'client', 'js', 'index.js'),
      path.join(__dirname, 'client', 'css', 'style.css')
    ],
    output: {
      filename: path.join('js', '[name]-[hash:6].js'),
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/'
    },

    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          use: { loader: 'babel-loader' }
        },
        {
          test: /\.css$/,
          use: isProd
            ? ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: ['css-loader']
            })
            : ['style-loader', 'css-loader']
        },
        {
          test: /\.(jpe?g|png|gif|svg)$/,
          use: ['file-loader?name=images/[name].[ext]']
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/,
          use: ['file-loader?name=fonts/[name]-[hash:6].[ext]']
        }
      ]
    },

    plugins: [
      new HtmlWebpackPlugin({
        title: 'Cholita',
        favicon: path.resolve(__dirname, 'client', 'assets', 'favicon.ico'),
        template: path.resolve(__dirname, 'client', 'index.html')
      }),
      new webpack.DefinePlugin({
        ENV: JSON.stringify(e)
      })
    ],

    devServer: {
      contentBase: path.resolve(__dirname, 'client')
    }
  }

  if (isProd) {
    config.plugins.push(new ExtractTextPlugin({
      filename: path.join('css', '[name]-[hash:6].css')
    }))
  }

  return config
}
